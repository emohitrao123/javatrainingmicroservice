package com.training.microservice;

import com.training.microservice.bean.LimitConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitConfigurationController {

    @Autowired
    private Configurator configurator;

    @GetMapping("/limit-service")

    private LimitConfiguration provideLimits(){
        return new LimitConfiguration(configurator.getMinimum(),configurator.getMaximum());
    }
}
